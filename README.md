# Plugin Sample

This is a sample plugin developed based on a developer challenge coding test to demonstrate my
plugin development skills.

## Installation

Create a folder in your WordPress plugin directory named "dev-challenge" and download the contents into
that directory, then activate the plugin from your admin login.


## Contributing

Scott DeLoach - scott@blessedlogic.com
