<?php
/**
 * Plugin Name:         Dev Challenge
 * Description:         MemberPress Developer Challenge
 * Version:             1.0.0
 * Author:              Scott DeLoach
 */

 /**
  * Global Declarations Section
  *
  * Declares namespace and JSON data file path
  */
namespace MPDevChallenge;
define( 'JSON_FILE', plugin_dir_path(__FILE__) . 'public/data/dev-challenge.json' );

/**
 * Enqueue Section
 *
 * Set up CSS and Javascript links for both backend and front end.
 *
 */
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\\dev_challenge_admin_style' );

function dev_challenge_admin_style() {

  wp_enqueue_style( 'dev-challenge-admin-style', plugins_url( 'dev-challenge/public/css/dev-challenge-admin.css' ) );
  wp_enqueue_style( 'dev-challenge-style', plugins_url( 'dev-challenge/public/css/dev-challenge.css' ) );
  wp_enqueue_script( 'dev-challenge-script', plugins_url( 'dev-challenge/public/js/dev-challenge.js' ) );

}

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\dev_challenge_style' );

function dev_challenge_style() {

  wp_enqueue_style( 'dev-challenge-style', plugins_url( 'dev-challenge/public/css/dev-challenge.css' ) );
  wp_enqueue_script( 'dev-challenge-script', plugins_url( 'dev-challenge/public/js/dev-challenge.js' ) );

}

/**
 * Admin Backend Section
 *
 * Set up top level menu and backend page to display table using JSON data.  Includes Refresh
 * button that will delete local JSON data file and reload page (which will pull fresh JSON data file
 * from the server).
 *
 */
add_action( 'admin_menu', __NAMESPACE__ . '\\mp_menu' );

function mp_menu() {

  // Create MemberPress top-level menu and link to MemberPress admin page
  add_menu_page( 'MemberPress', 'MemberPress', 'manage_options', 'mp-options',
  __NAMESPACE__ . '\\mp_admin_page', plugins_url( 'dev-challenge/public/img/mp-icon-WHITE.png' ), 99 );
}

function mp_admin_page() {
  $json_api_link = get_rest_url() . 'dev-challenge/v1/get-json/';

  // Triggered by fresh button.
  if ( isset( $_POST['action'] ) && $_POST['action'] == 'refresh' ) {
    reset_json_cli(); // Deletes local JSON data file to force new copy to be retrieved from server
  }

?>
<div id='mp-admin-header'>
<img src='<?php echo plugins_url( 'dev-challenge/public/img/mp-logo-horiz-RGB.jpg' ); ?>' width='300px'>
</div>
<?php

  echo runapi(); // Loads javascript that pulls json data and builds data table
  echo "<form method='post' action=''>";
  echo "<input type='hidden' name='action' value='refresh'>";
  submit_button( 'Refresh Data','primary' );
  echo "</form>";
  echo "<div id='mp-instructions'>";
  echo "<h3>Instructions:</h3><ul>";
  echo "<li>To insert this table on a post or page, use shortcode <b>[dev_challenge]</b></li>";
  echo "<li>To view the data in raw JSON, visit <a href='$json_api_link' target='_blank'>$json_api_link</a></li>";
  echo "</ul></div>";

}

/**
 * API Section
 *
 * Registers WordPress API to supply JSON data.  JSON data is always retrieved
 * from a local copy stored in dev-challenge/public/data/dev-challenge.json unless
 * the local file is over 1 hour old, then the local copy is overwritten by the
 * server copy located at https://cspf-dev-challenge.herokuapp.com.
 *
 * The JSON data can be viewed at [your-domain]/wp-json/dev-challenge/v1/get-json/
 *
 */

add_action( 'rest_api_init', __NAMESPACE__ . '\\get_json_api' );

function get_json_api() {

  register_rest_route( 'dev-challenge/v1','get-json',
    array(
      'methods' => 'GET',
      'callback' => __NAMESPACE__ . '\\get_json_data'
    )
  );

}

function get_json_data() {

  if ( file_exists( JSON_FILE ) && (  (time() - filemtime( JSON_FILE ) ) < 3600 ) ) {
    // If file exists and is less than 1 hour (3600 minutes) old, use local JSON data file.
    $jsondata = sanitize_text_field( file_get_contents( JSON_FILE ) );
  } else {
    // Else, update local JSON data file from the server
    $jsondata = sanitize_text_field( file_get_contents( "https://cspf-dev-challenge.herokuapp.com/" ) );
    file_put_contents( JSON_FILE, $jsondata );
  }

  echo $jsondata;

}

/**
 * WP CLI Section
 *
 * Registers WP_CLI command 'reset-json' which will delete the local copy so that
 * the next time the API is called, a new copy of the JSON data will be created
 * from the server.
 *
 */

if ( class_exists( 'WP_CLI' ) ) { // Only need to register if this is being called from WP_CLI
  \WP_CLI::add_command( 'reset-json', __NAMESPACE__ . '\\reset_json_cli' );
}

function reset_json_cli() {

  unlink( JSON_FILE );
  if ( class_exists( 'WP_CLI' ) ) {
    \WP_CLI::success( 'JSON file has been reset.' );
  }

}

/**
 * Shortcode Section
 *
 * Registers a short code to be used with a post or page, which will insert an
 * AJAX script that calls the REST API to pull the JSON data and populate the
 * data table on the page.  ob_start() and ob_get_clean() functions are called
 * here to prevent the table from appearing on the editor page where the
 * shortcode exists.
 *
 * Usage:  [dev_challenge]
 *
 */

add_shortcode( 'dev_challenge', __NAMESPACE__ . '\\runapi' );

function runapi() {

  ob_start();
?>
<div id='dev-challenge-table'></div>

<script>
var xhttp = new XMLHttpRequest();
xhttp.open( "GET", "<?php echo get_rest_url(); ?>dev-challenge/v1/get-json/", true );
xhttp.send();
xhttp.onreadystatechange = function () {
  if ( this.readyState == 4 && this.status == 200 ) {
    createtable( this.response );
  }
};
</script>
<noscript><p>ERROR:  Javascript needs to be enabled.</p></noscript>
<?php

  return ob_get_clean();

}

?>
