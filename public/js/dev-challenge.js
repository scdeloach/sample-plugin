// Format timestamp to human readable date/time
function formatdate(indate) {
  var datestring = '';
  var now = new Date(indate * 1000);
  datestring += (now.getMonth() + 1) + '/';
  datestring += now.getDate() + '/';
  datestring += now.getFullYear() + ' ';
  datestring += now.getHours() + ':';
  datestring += now.getMinutes() + ':';
  datestring += now.getSeconds();

  return datestring;
}

// Generate HTML table from JSON data
function createtable(indata) {
  var outdata = '';
  var jsondata = JSON.parse(indata);
  var title = jsondata['title'];
  var headers = jsondata['data']['headers'];
  var rows = jsondata['data']['rows'];

  // Build table
  outdata += "<h2>" +  title + "</h2><table><tr>";
  for (let index in headers) { outdata += "<th>" + headers[index] + "</th>"; };
  for (let index1 in rows) {
    outdata += "</tr><tr>";
    var rowdata = rows[index1];
    for (let index2 in rowdata) {
      outdata += "<td>" + (index2 == 'date' ? formatdate(rowdata[index2]) : rowdata[index2]) + "</td>";
    };
  };
  outdata += "</tr></table>";

  document.getElementById("dev-challenge-table").innerHTML = outdata;
}
